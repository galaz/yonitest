<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class AddruleController extends Controller
{

	public function actionOwnCategory()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnCategoryRule;
		$auth->add($rule);
	}
}
