<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m170718_113453_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('category');
    }
}